
var eeg_data ={
  labels: [
    {% set t = namespace(value=0) %}
    {% set T = 1/173.61 %}
    {% for point in data %}
      {{ "%.2f"|format(t.value) }},
      {% set t.value = t.value+T %} 
    {% endfor %}
  ],
  datasets: [{
    data: [
      {% for point in data %}
        {% if point %} {{ point }}, {% endif %}
      {% endfor %}
    ],
    borderColor:['rgb(0,10,250)'],
  }]
}; 


var eeg_chart = document.getElementById('eeg_canvas');
if (eeg_chart) {
  new Chart(eeg_chart, {
    type: 'line',
    data: eeg_data,
    options: {
      elements: {
        point:{
          radius: 0
        },
        line: {
          fill: false,
          borderWidth: 1
        }
      },
      title: {
        display: false,
        text: 'EEG'
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          ticks: {
            callback: function(tick, index, array) {
              return (index % 4) ? "" : tick;
            }
          },
          gridLines: {
            display: false
          },
          scaleLabel: {
            display: true,
            labelString: 'tiempo'
          },
        }],
        yAxes: [{
          gridLines: {
            display:true
          },
          scaleLabel: {
            display: true,
            labelString: 'mV'
          },
          ticks: {
            beginAtZero: false
          }
        }],
      }
    }
  });
};