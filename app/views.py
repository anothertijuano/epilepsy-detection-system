# views.py

from flask import make_response, render_template, request
from app import app
from clasificador import clasificador, test
filePath = './Uploads/egg.txt'

modelo = clasificador()

@app.route('/')
def home():
    resp=make_response(render_template("home.j2"))
    resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    resp.headers["Pragma"] = "no-cache"
    resp.headers["Expires"] = "0"
    return resp

@app.route('/success', methods = ['GET', 'POST'])
def success():  
    if request.method == 'POST':
        filename=filePath
        f = request.files['file']
        f.save(filename)
        txt=''
        prediction = -1 #Default value
        with open(filePath, 'r', encoding='utf8', errors='ignore') as eeg:
            txt = eeg.read()
            prediction = test(modelo, filePath)
        muestras = sum(1 for line in txt.split('\n'))
        print(muestras)
        resp=make_response(render_template("selector.j2", prediction=prediction, data=txt.split('\n'), muestras=muestras))
        return resp
    return render_template("error.j2") 

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('error.j2'), 404