import matplotlib.pyplot as plt
import os
import re
import shutil
import string
import numpy as np

from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score


from rasgos import wavelet

def clasificador():
    ###
    ### Datos
    ###

    DatasetPath = './Dataset/'
    #Select dataset
    paths_true = os.listdir(DatasetPath) #List all txt in S
    paths_false = os.listdir(DatasetPath) #List all txt in Z

    data_training = []
    data_training_label = []
    data_validation = []
    data_validation_label = []
    data_testing = []
    data_testing_label = []

    # Split dataset
    # True (epilepsy positive)
    paths_true_training = paths_true[0:50] #Primeros 50 elementos de true para testing
    paths_true_validation = paths_true[50:100] #Ultimos 25 elementos de true para validation
    paths_true_testing = paths_true[75:100] #Ultimos 255 elementos de true para validation


    for path in paths_true_training: # Para cada dirección de archivo en archivos de entrenamiento
        data_training.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training
        data_training_label.append(1) # Agregar etiqueta 

    for path in paths_true_validation: # Para cada dirección de archivo en archivos de entrenamiento
        data_validation.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training 
        data_validation_label.append(1) # Agregar etiqueta 
    '''
    for path in paths_true_testing: # Para cada dirección de archivo en archivos de entrenamiento
        data_testing.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training 
        data_testing_label.append(1) # Agregar etiqueta 
    '''

    # False (epilepsy negative)
    paths_false_training = paths_false[100:150] #Primeros 50 elementos de false para testing
    paths_false_validation = paths_false[150:200] #Ultimos 50 elementos de false  para validation
    paths_false_testing = paths_false[175:200] #Ultimos 50 elementos de false  para validation

    for path in paths_false_training: # Para cada dirección de archivo en archivos de entrenamiento
        data_training.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training 
        data_training_label.append(0) # Agregar etiqueta 


    for path in paths_false_validation: # Para cada dirección de archivo en archivos de entrenamiento
        data_validation.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training
        data_validation_label.append(0) # Agregar etiqueta 
    '''
    for path in paths_false_testing: # Para cada dirección de archivo en archivos de entrenamiento
        data_testing.append(wavelet(open(DatasetPath+path))) # Agregar rasgos a data_training
        data_testing_label.append(0) # Agregar etiqueta 
    '''

    data_training  = np.array(data_training )
    data_training_label  = np.array(data_training_label )
    data_validation = np.array(data_validation)
    data_validation_label  = np.array(data_validation_label )

    points_1 = data_training[0:50] + data_validation[0:50]
    points_2 = data_training[50:100] + data_validation[50:100]


    ###
    # Plot de puntos verdaderos y falsos
    ###
    '''
    #plt.figure(figsize=(16,9))
    plt.scatter(points_1[:,0],points_1[:,1],color='red',label='Verdaderos')
    plt.scatter(points_2[:,0],points_2[:,1],color='black',label='Falsos')
    plt.legend()
    plt.show()
    '''

    # =============================================================================
    # Make them as numpy arrays 
    # =============================================================================
    x_train = np.array(data_training)
    y_train = np.array(data_training_label)
    x_test = np.array(data_validation)
    y_test = np.array(data_validation_label)

    # =============================================================================
    # Counting each class
    # =============================================================================
    class0=0
    class1=0
    for n in range(0, y_test.shape[0]):
        if(y_test[n] == 0):
            class0 = class0+1
        elif(y_test[n] == 1):
            class1 = class1+1

    # =============================================================================
    #   Magia
    # =============================================================================
    clf = SVC(kernel='linear')
    clf.fit(x_train,y_train)

    '''
    y_pred = clf.predict(x_test)

    modelSVMScore = clf.score(x_test, y_test)
    print('Model Test Accuracy: %1.4f' % (modelSVMScore))

    modelSVMAccuracyScore = accuracy_score(y_test, y_pred)
    print('Model Prediction Accuracy: %1.4f' % (modelSVMAccuracyScore))



    modelSVMConfusionMatrix=confusion_matrix(y_test, y_pred, labels=[0, 1])
    print('\nConfusion matrix: Rows Actual class, Columns Predicted class.')
    print('Class_0: Verdadero = %d, Class_1: False = %d. \nTotal test samples %d.\n' % (class0, class1, y_test.shape[0] ) )
    print(modelSVMConfusionMatrix)
    '''

    return(clf)

def test(modelo, filepath):
    vector = wavelet(open(filepath))
    clase = modelo.predict([[vector[0], vector[1]]])
    return(clase[0])