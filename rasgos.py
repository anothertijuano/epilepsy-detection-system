from pywt import cwt
from numpy import arange
import matplotlib.pyplot as plt

def str2int(str):
    try:
        return(int(str))
    except:
        return(False)

def filereader(txtfile):
    data=[]
    # Agrega cada numero del archivo a la lista data
    for line in txtfile.read().split('\n'):
        number = str2int(line)
        if number:
            data.append(number)
    return(data)
    
def wavelet(txtfile):
    # Obtener el promedio de cada nivel
    # Regresa lista de tupla de rasgos -> list( (list(), list()) )
    
    maxScale = 8 
    minScale = 1

    data = filereader(txtfile)
    coefc, freqs = cwt(data, arange(minScale,maxScale+minScale), 'gaus1')

    #Filtrar números complejos    
    coefj = 'Real'
    if isinstance (coefc[0,0], complex):
        coef = abs(coefc)
        coefj = 'Complex'
    else:
        coefj = 'Real'
        coef = coefc

    #Promedios
    promedios = []

    ## 2 y 3
    ## 5, 6 y 7

    n = len(coef[0]) #cantidad de coeficientes
    s = 0

    rasgo1 = []
    for array in coef[2:4]:
        s=max(array)
        rasgo1.append(s)

    rasgo2 = []
    for array in coef[5:8]:
        s=min(array)
        rasgo2.append(s)
    
    #rasgos = ( (rasgo1[0]+rasgo1[1])/2, (rasgo2[0]/rasgo2[1])/2 )
    rasgos = ( max(rasgo1), min(rasgo2) )
    return(rasgos)