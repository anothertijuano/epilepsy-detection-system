from rasgos import fourier
import os
import numpy as np
import random

###
### Datos
###
print('### Datos ###')

###coefi = coef(open('./Uploads/egg.txt'))

DatasetPath = './Dataset/'
#Select dataset
paths_true = os.listdir(DatasetPath) #List all txt in S
paths_false = os.listdir(DatasetPath) #List all txt in Z

data_training = []
data_training_label = []
data_validation = []
data_validation_label = []
data_testing = []
data_testing_label = []
# Split dataset
# True (epilepsy positive)
paths_true_training = paths_true[0:50] #Primeros 50 elementos de true para testing
paths_true_validation = paths_true[50:75] #Ultimos 25 elementos de true para validation
paths_true_testing = paths_true[75:100] #Ultimos 255 elementos de true para validation


for path in paths_true_training: # Para cada dirección de archivo en archivos de entrenamiento
    data_training.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training
    data_training_label.append(1) # Agregar etiqueta 

for path in paths_true_validation: # Para cada dirección de archivo en archivos de entrenamiento
    data_validation.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training 
    data_validation_label.append(1) # Agregar etiqueta 

for path in paths_true_testing: # Para cada dirección de archivo en archivos de entrenamiento
    data_testing.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training 
    data_testing_label.append(1) # Agregar etiqueta 

# False (epilepsy negative)
paths_false_training = paths_false[0:50] #Primeros 50 elementos de false para testing
paths_false_validation = paths_false[50:75] #Ultimos 50 elementos de false  para validation
paths_false_testing = paths_false[75:100] #Ultimos 50 elementos de false  para validation

for path in paths_false_training: # Para cada dirección de archivo en archivos de entrenamiento
    data_training.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training 
    data_training_label.append(0) # Agregar etiqueta 


for path in paths_false_validation: # Para cada dirección de archivo en archivos de entrenamiento
    data_validation.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training
    data_validation_label.append(0) # Agregar etiqueta 

for path in paths_false_testing: # Para cada dirección de archivo en archivos de entrenamiento
    data_testing.append(fourier(open(DatasetPath+path))) # Agregar rasgos a data_training
    data_testing_label.append(0) # Agregar etiqueta 


#Shuffle
tmp = list(tuple(zip(data_training, data_training_label)))
random.shuffle(tmp)
data_training, data_training_label = zip(*tmp)

tmp = list(tuple(zip(data_validation, data_validation_label)))
random.shuffle(tmp)
data_validation, data_validation_label = zip(*tmp)

data_training  = np.array(data_training )
data_training_label  = np.array(data_training_label )
data_validation = np.array(data_validation)
data_validation_label  = np.array(data_validation_label )


# Importando el modelo
from sklearn.linear_model import LogisticRegression

rlog = LogisticRegression() # Creando el modelo


rlog.fit(data_training, data_training_label) #ajustando el modelo

# Realizando las predicciones
y_predic_entrenamiento = rlog.predict(data_training) 
y_predic_evaluacion = rlog.predict(data_validation)

# Verificando la exactitud del modelo
entrenamiento = (y_predic_entrenamiento == data_training_label).sum().astype(float) / data_training_label.shape[0]
print("sobre datos de entrenamiento: {0:.2f}".format(entrenamiento))
evaluacion = (y_predic_evaluacion == data_validation_label).sum().astype(float)/data_validation_label.shape[0]
print("sobre datos de evaluación: {0:.2f}".format(evaluacion))
